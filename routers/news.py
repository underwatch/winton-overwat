from fastapi import APIRouter
from urllib import request
import json
import re

router = APIRouter(
    tags=["news"],
    responses={404: {"description": "Not found"}},
)
domain = 'https://overwatch.blizzard.com/'


@router.get("/{lang}/news")
async def news(lang):
    cards = ''
    req = request.Request('{}{}/news'.format(domain, lang))
    parsed = request.urlopen(req).read().decode("utf8")
    result = re.findall(r"<blz-card[^>]*>.+?<\/blz-card>", parsed)

    convert = []

    for values in result:
        d = {
            'title': re.findall(r"(?<=<h4 slot=\"heading\">).+?(?=<\/h4>)", values)[0],
            'date': re.findall(r"(?<=date=\").*?(?=\")", values)[0],
            'url': '{}{}/news{}'.format(domain, lang, re.findall(r"(?<=href=\").*?(?=\")", values)[0]),
            'image': 'https:{}'.format(re.findall(r"(?<=src=\").*?(?=\")", values)[0])
        }

        convert.append(d)
    
    return convert