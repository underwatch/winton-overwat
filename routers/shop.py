from fastapi import APIRouter
from urllib import request
import json

router = APIRouter(
    tags=["shop"],
    responses={404: {"description": "Not found"}},
)
domain = 'https://kr.shop.battle.net'


@router.get("/{lang}")
async def shop(lang):
    headers = {
        'accept-language': lang
    }
    req = request.Request('{}/api/itemshop/overwatch/pages/6a011f23-5874-4df5-a38f-1086f6c636f6?userId=0&locale={}'.format(domain, lang), headers=headers)
    contents = json.loads(request.urlopen(req).read())

    items = []

    for values in contents['mtxCollections'][0]['items']:
        d = {
            'title': values['title'],
            'description': values['description'],
            'price': values['price']['fullAmount'],
            'image': values['image']['url'],
            'url': '{}{}'.format(domain, values['destination'])
        }
        items.append(d)

    return items