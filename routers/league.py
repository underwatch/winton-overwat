from fastapi import APIRouter
from urllib import request
import json
import re

router = APIRouter(
    tags=["league"],
    responses={404: {"description": "Not found"}},
)
domain = 'https://overwatchleague.com/'


@router.get("/{lang}/league")
async def league(lang):
    headers = {
        'accept-language': lang
    }
    req = request.Request('{}{}/cosmetics'.format(domain, lang))
    parsed = request.urlopen(req).read().decode("utf8")
    result = re.findall(r"(?<=<script id=\"__NEXT_DATA__\" type=\"application\/json\">)(.*?)(?=<\/script>)", parsed)
    myjson = json.loads(result[0])['props']['pageProps']['blocks'][2]['virtualCurrency']['cards']
    items = []

    for values in myjson:
        d = {
            'title': values['title'],
            'image': values['image']['src'],
            'team': values['team']['name'],
            'price': values['price']
        }

        items.append(d)

    return items
