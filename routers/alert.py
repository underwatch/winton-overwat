from fastapi import APIRouter
from urllib import request
import json
import re

router = APIRouter(
    tags=["alert"],
    responses={404: {"description": "Not found"}},
)
domain = 'https://alert-kr.battle.net/'


@router.get("/{lang}/alert")
async def alert(lang):
    headers = {
        'accept-language': lang
    }
    req = request.Request('{}App/default/live-maintenance/{}'.format(domain, lang))
    parsed = request.urlopen(req).read().decode("utf-8")
    
    if parsed is '\n':
        return {"content": None}
    else:
        return {"content": parsed}
